from Aeros import WebServer, render_template, jsonify
from time import strftime

app = WebServer(__name__, host="0.0.0.0", port=80)  # init backend & web server
app.template_folder = "./frontend/build/"
app.static_folder = "./frontend/build/static/"


@app.route("/")
async def home():
    return await render_template("index.html")


@app.route("/time/")
async def current_time():
    return jsonify({"timestamp": strftime("%H:%M:%S")})


app.run_server()  # run web server in this thread (endless mode)
