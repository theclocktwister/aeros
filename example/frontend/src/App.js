import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {jsonRequest} from 'aeros-frontend';


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {currentTime: "NA"}
    }


    componentDidMount() {
        jsonRequest('http://127.0.0.1/time/', console.log, {method: 'GET'}).then((data) => {
            this.setState({currentTime: data.timestamp})
        })

        setInterval(() => {
            jsonRequest('http://127.0.0.1/time/', console.log, {method: 'GET'}).then((data) => {
                this.setState({currentTime: data.timestamp})
            })
        }, 1000)
    }


    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <p>It's currently {this.state.currentTime}.</p>
                </header>
            </div>
        );
    }
}

export default App;
