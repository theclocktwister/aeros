![](https://gitlab.com/wiesen.leon/aeros/-/raw/master/doc/header.png)

<p align="center">
  <img src="https://img.shields.io/pypi/pyversions/Aeros?label=Python%20Version&style=flat-square">
  <img src="https://img.shields.io/pypi/v/Aeros?label=PyPi%20Version&style=flat-square"/>
  <img src="https://img.shields.io/pypi/format/Aeros?label=PyPi%20Format&style=flat-square"/>
  <img src="https://img.shields.io/pypi/dm/Aeros?label=Downloads&style=flat-square"/>
  <img src="https://img.shields.io/github/repo-size/TheClockTwister/Aeros?label=Repo%20Size&style=flat-square">
</p>

# npm package documentation
[aeros-frontend](https://www.npmjs.com/package/aeros-frontend) is a package for handling the
frontend-backend communication between your ReactJS / JavaScript frontend and your
[Aeros](https://pypi.org/project/Aeros/) backend.
