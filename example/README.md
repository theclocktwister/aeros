# Example code

### Project hierarchy
- `example/`
  - `frontend/` - Contains your ReactJS application 
  - `webserver.py` - Python entry point (contains backend and web server)

### Explanation
The `frontend/` folder will store your frontend, while you may develop your
Python web server (and its backend) in a Python file or a separate folder
(`backend/` for example). This will provide you the most clear and lucid
project structure possible and ensure, that you can work on frontend functions
and their respective backend APIs without any trouble .