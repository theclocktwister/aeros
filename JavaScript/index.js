async function jsonRequest(url, onError, ...fetchParams) {

    const r = await fetch(url, ...fetchParams)
    if (r.ok) {
        return await r.json()
    } else {
        return r
    }
}

export {jsonRequest}
