Aeros Python Documentation
===========================================================

.. _Aeros: https://pypi.org/project/Aeros/
.. _`GitLab Repository`: https://gitlab.com/wiesen.leon/aeros
.. _`PyPi Package`: https://pypi.org/project/Aeros/
.. _`npm Package`: https://www.npmjs.com/package/aeros-frontend


Aeros_ is a production-grade ASGI (Asynchronous Server Gateway Interface) package
containing wrappers for widely used Web and API functions.

.. admonition:: Contributors wanted

    Please keep in mind that this package including all source code and documentation
    is developed and maintained by a single person. I am investing a large amount of
    time into maintaining this project in order to supply the most intuitive, yet powerful
    high-level API possible.

    **Any collaborative contributions, especially improvements and fixes are appreciated
    and should be inquired via an issue at the** `GitLab Repository`_.






Aeros Repository Links
-----------------------------------------------------------
- :ref:`genindex`
- `GitLab Repository`_
- `PyPi Package`_
- `npm Package`_ (comming soon)





..
    This is a comment





.. toctree::
    :maxdepth: 1
    :caption: General
    :glob:
    :hidden:

    content/*

.. toctree::
    :maxdepth: 2
    :caption: Code Examples
    :glob:
    :hidden:

    content/examples/index

.. toctree::
    :maxdepth: 1
    :caption: Full API Documentation
    :hidden:
